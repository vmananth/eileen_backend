from authentication.models import UserInfo
from search.models import Vote, Document, TitleKeyword

from elasticsearch_dsl.connections import connections
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from django.http import Http404
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.conf import settings
import jwt
import numpy as np
import operator
import rake
import re
from collections import defaultdict

connections.create_connection(hosts=[settings.ELASTIC_HOST], timeout=20)
client = Elasticsearch([settings.ELASTIC_HOST])

@api_view(['GET'])
def search(_, search_string):
    s = Search().using(client).query(settings.SEARCH_KEY, _all=search_string)
    s = s.source(["id", "source", "sourceID", "documentType","title", "venue", "summary", "scientists",
                  "organizations", "date"])
    search_result = s.execute()
    return Response(search_result.hits.hits)


@api_view(['GET'])
def dummy_api(_):
    s = Search().using(client).query(settings.SEARCH_KEY, _all='a')
    search_result = s.execute()
    return Response(search_result.hits.hits)


@api_view(['GET'])
def get_document(_, document_id):
    s = Search().using(client).query(settings.MATCHALL_KEY, id=document_id)
    s = s.source(["id", "source", "sourceID", "documentType", "title", "venue", "summary", "scientists",
                  "organizations", "date"])
    search_result = s.execute()    
    return Response(search_result.hits.hits)


@api_view(['POST'])
def vote(request):
    document_id = request.data.get('document_id')
    vote_type = request.data.get('type')
    request_token = request.META["HTTP_AUTHORIZATION"].split()[1]
    token = parse_token(request_token)
    if UserInfo.objects.filter(id=int(token['id'])).exists():
        user = UserInfo.get_id(int(token['id']))
        Vote.get_vote(user, document_id, vote_type)
        return Response({"success": "success"})
    else:
        raise Http404("user does not exist")


@api_view(['GET'])
def recommend(request):
    request_token = request.META["HTTP_AUTHORIZATION"].split()[1]
    token = parse_token(request_token)
    if UserInfo.objects.filter(id=int(token['id'])).exists():
        user = UserInfo.get_id(int(token['id']))
        relevant = list(Vote.objects.filter(user=user, type="R").values_list('document_id', flat=True))
        irrelevant = list(Vote.objects.filter(user=user, type="I").values_list('document_id', flat=True))
        document_list = recommend_engine(relevant, irrelevant)

        #recommendation_keyword_list = extract_recommendation_words(relevant, irrelevant) # Extract keywords from user's
                                                                                         # relevant and irrelevant list
        return Response({"document list": document_list})
    else:
        raise Http404("user does not exist")


def recommend_engine(relevant_documents, irrelevant_documents):
    document_list = []
    topic_norm__r = []
    topic_norm__i = []

    # get topicNorm for relevant and irrelevant documents

    s = Search.from_dict({
        "query": {
            "ids": {
                "values": relevant_documents
            }
        }
    })
    relevant_result = s.execute()
    for result in relevant_result.hits.hits:
        topic_norm__r.append(result["_source"]["topicNorm"])
    s = Search.from_dict({
        "query": {
            "ids": {
                "values": irrelevant_documents
            }
        }
    })
    irrelevant_result = s.execute()
    for result in irrelevant_result.hits.hits:
        topic_norm__i.append(result["_source"]["topicNorm"])

    # get sum of topicNorm for relevant and irrelevant separately
    sum_topic_norm__r = np.sum(topic_norm__r, axis=0).tolist()
    sum_topic_norm__i = np.sum(topic_norm__i, axis=0).tolist()

    vector = [0] * 150
    # alpha = 1.6 and beta = 0.2 to get vector
    # check if divided by 0
    if len(topic_norm__r) == 0 and len(topic_norm__i) == 0:
        return document_list
    if len(topic_norm__i) == 0:
        for x in range(0, 150):
            vector[x] = settings.ALPHA * sum_topic_norm__r[x] / len(topic_norm__r)
    elif len(topic_norm__r) == 0:
        for x in range(0, 150):
            vector[x] = sum_topic_norm__r - settings.BETA*sum_topic_norm__i[x]/len(topic_norm__i)
    else:
        for x in range(0, 150):
            vector[x] = settings.ALPHA*sum_topic_norm__r[x]/len(topic_norm__r) - \
                        settings.BETA*sum_topic_norm__i[x]/len(topic_norm__i)
    # calculate bucket using vector
    components = load_random_components()
    query_vector = np.array(vector)
    buckets = np.dot(query_vector, components)
    buckets = np.sign(buckets)
    buckets = [int(x) for x in buckets]
    buckets_string = str(buckets).replace(" ", "")
    # get recommend documents from same bucket
    document_list = get_recommend_documents(buckets_string, vector)
    return document_list


# get recommend documents from same bucket and sort with cos(vector)
def get_recommend_documents(buckets_string, vector):
    s = Search.from_dict({
        "query": {
            "term": {
                "bucketsText.keyword": buckets_string
            }
        }
    })
    search_result = s.execute().hits.hits
    recommend_document = {}
    # calculate the cos distance from vector for each document in the recommendation
    for each in range(0, len(search_result)):
        cos_tmp = sum([search_result[each]["_source"]["topicNorm"][i]*vector[i] for i in range(0, 150)])
        recommend_document[each] = cos_tmp
        # remove fields we don't want to show at front end
        del search_result[each]["_source"]["buckets"]
        del search_result[each]["_source"]["bucketsText"]
        del search_result[each]["_source"]["topicNorm"]

    # sort all recommendation document based on cos distance
    sorted_d = sorted(recommend_document.items(), key=operator.itemgetter(1))
    sorted_d.reverse()
    document_list = [search_result[x[0]] for x in sorted_d]
    return document_list


# get random component for calculate bucket using vector
def load_random_components():
    f = open(settings.RANDOM_PATH)
    f.readline() # read column names
    line = f.readline()
    tmp = []
    while line:
        tmp = tmp + [float(x) for x in line.split(',')]
        line = f.readline()
    a = np.array(tmp).reshape((150, 20))
    f.close()
    return a


@api_view(['GET'])
def clear_vote(request, document_id):
    request_token = request.META["HTTP_AUTHORIZATION"].split()[1]
    token = parse_token(request_token)
    # delete vote if exists, or don't do anything
    if UserInfo.objects.filter(id=int(token['id'])).exists():
        user = UserInfo.get_id(int(token['id']))
        Vote.objects.filter(user=user, document_id=document_id).delete()
        return Response({"delete success": document_id})
    else:
        raise Http404("user does not exist")


@api_view(['GET'])
def clear_all(request):
    request_token = request.META["HTTP_AUTHORIZATION"].split()[1]
    token = parse_token(request_token)
    if UserInfo.objects.filter(id=int(token['id'])).exists():
        user = UserInfo.get_id(int(token['id']))
        Vote.objects.filter(user=user).delete()
        return Response({"delete success": "all"})
    else:
        raise Http404("user does not exist")


@api_view(['GET'])
def extract_recommendation_words(request):
    request_token = request.META["HTTP_AUTHORIZATION"].split()[1]
    token = parse_token(request_token)

    if UserInfo.objects.filter(id=int(token['id'])).exists():
        user = UserInfo.get_id(int(token['id']))
        relevant = list(Vote.objects.filter(user=user, type="R").values_list('document_id', flat=True))
        irrelevant = list(Vote.objects.filter(user=user, type="I").values_list('document_id', flat=True))

        relevant_titles = []
        irrelevant_titles = []
        relevant_keywords_list = []
        irrelevant_keywords_list = []

        for each in relevant:
            s = Search().using(client).query(settings.MATCHALL_KEY, id=each)
            search_result = s.execute()
            relevant_titles.append(search_result.hits.hits[0]["_source"]['title'])
        for each2 in irrelevant:
            s = Search().using(client).query(settings.MATCHALL_KEY, id=each2)
            search_result = s.execute()
            irrelevant_titles.append(search_result.hits.hits[0]["_source"]['title'])
        relevant_keywords = title_keyword_extract(relevant_titles)
        irrelevant_keywords = title_keyword_extract(irrelevant_titles)

        for keyword in relevant_keywords:
            if 2 <= keyword['score'] <= 6:      # Check if keyword score is between 2 and 6
                relevant_keywords_list.append(keyword['keyword'])
        for keyword in irrelevant_keywords:
            if 2 <= keyword['score'] <= 6:  # Check if keyword score is between 2 and 6
                irrelevant_keywords_list.append(keyword['keyword'])
        recommendation_keyword_list = set(relevant_keywords_list) - set(irrelevant_keywords_list)
        keyword_objects = list(TitleKeyword.objects.filter(keyword__in = recommendation_keyword_list).values())
        groups = defaultdict(list)
        for obj in keyword_objects:
            groups[obj['keyword']].append(obj)              # Group objects having similar 'keyword' in a list
            recommendation_keyword_list = groups.values()
        return Response({"recommendation_keyword_list": recommendation_keyword_list})
    else:
        raise Http404("user does not exist")


def title_keyword_extract(text_list):
    keyword_list = []
    rake_object = rake.Rake("SmartStoplist.txt", 3, 3, 1)
    for text in text_list:
        text = re.sub('[^A-Za-z0-9 ]+', '', text)
        keywords_tuple = rake_object.run(text)
        for i in range(0,len(keywords_tuple)):
            keyword_dict = dict()
            keyword_dict['keyword'] = keywords_tuple[i][0]
            keyword_dict['score'] = keywords_tuple[i][1]
            keyword_list.append(keyword_dict)
    return keyword_list


def parse_token(token):
    return jwt.decode(token, settings.SECRET_KEY)

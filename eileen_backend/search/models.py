from django.db import models
from django.contrib.auth.models import User
from authentication.models import UserInfo
from django.core.exceptions import ObjectDoesNotExist


VOTE_CHOICES = [
    ("R", "Relevant"),
    ("I", "Irrelevant")
]


class Vote(models.Model):
    user = models.ForeignKey(UserInfo, related_name='votes', on_delete=models.CASCADE)
    document_id = models.CharField(max_length=100, default="")
    type = models.CharField(max_length=1, choices=VOTE_CHOICES, default='R')

    @staticmethod
    def get_vote(user, document_id, type):
        # if vote already there, update it
        if Vote.objects.filter(user=user, document_id=document_id).exists():
            vote = Vote.objects.get(user=user, document_id=document_id)
            vote.type = type
            vote.save()
        else:
            vote = Vote(user=user, document_id=document_id, type=type)
            vote.save()
        return vote


class Document(models.Model):
    title = models.CharField(max_length=50, default="")
    identifier = models.CharField(max_length=100, default="")
    source = models.CharField(max_length=100, default="")
    source_id = models.CharField(max_length=100, default="")
    type = models.CharField(max_length=30, default="")
    venue = models.CharField(max_length=30, default="")
    abstract = models.CharField(max_length=1000, default="")
    scientists = models.CharField(max_length=100, default="")
    organizations = models.CharField(max_length=30, default="")
    date = models.DateField(default=None)
    end_date = models.DateField(default=None)
    city = models.CharField(max_length=20, default="")
    country = models.CharField(max_length=20, default="")


class TitleKeyword(models.Model):
    year = models.IntegerField(null=True)
    keyword = models.TextField(max_length=200, default="")
    frequency = models.IntegerField(null=True)
    popularity = models.FloatField(null=True)
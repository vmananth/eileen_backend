# Create your models here.
from django.db import models


class UserInfo(models.Model):
   username = models.CharField(max_length=100)
   social_type = models.CharField(max_length=100)
   social_id = models.CharField(max_length=100)
   email = models.EmailField()
   first_name = models.CharField(max_length=100)
   last_name = models.CharField(max_length=100)

   @staticmethod
   def get_user(socialtype, socialid, uname, fname, lname, mail):
       if UserInfo.objects.filter(social_id=socialid, social_type=socialtype).exists():
           return UserInfo.objects.get(social_id=socialid, social_type=socialtype)
       else:
           user = UserInfo(username=uname, email=mail, first_name=fname, last_name=lname, social_type=socialtype,
                           social_id=socialid)
           user.save()
           return user

   @staticmethod
   def get_id(user_id):
       user = UserInfo.objects.get(id=user_id)
       return user
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
import json
import requests
import jwt
from datetime import datetime, timedelta

from authentication.models import UserInfo
from django.conf import settings


@api_view(['POST'])
def facebook(request):
    access_token_url = settings.FB_ACCESS_TOKEN_URL
    graph_api_url = settings.FB_USER_DATA_URL

    body_unicode = request.body.decode('utf-8')
    body_data = json.loads(body_unicode)

    access_token_params = {
        'client_id': body_data['clientId'],
        'redirect_uri': body_data['redirectUri'],
        'client_secret': settings.FB_SECRET_KEY,
        'code': body_data['code']
    }

    # Step 1. Exchange authorization code for access token.
    response_access_token = requests.get(access_token_url, params=access_token_params)
    response_access_token = json.loads(response_access_token.content.decode())

    user_data_params = {
        'access_token': response_access_token['access_token'],
        'fields': 'first_name, last_name, name, email'
    }

    # Step 2. Retrieve information about the current user.
    response_user_data = requests.get(graph_api_url, params=user_data_params)
    response_user_data = json.loads(response_user_data.content.decode())

    user = UserInfo.get_user(settings.FB_MODEL, response_user_data['id'], response_user_data['name'],
                             response_user_data['first_name'], response_user_data['last_name'],
                             response_user_data['email'])

    token = create_token(user)
    return Response({'token': token}, status=status.HTTP_200_OK)


@api_view(['POST'])
def google(request):
    access_token_url = settings.GOOGLE_ACCESS_TOKEN_URL
    people_api_url = settings.GOOGLE_USER_DATA_URL

    body_unicode = request.body.decode('utf-8')
    body_data = json.loads(body_unicode)

    access_token_payload = dict(client_id=body_data['clientId'],
                                redirect_uri=body_data['redirectUri'],
                                client_secret=settings.GOOGLE_SECRET_KEY,
                                code=body_data['code'],
                                grant_type='authorization_code')

    # Step 1. Exchange authorization code for access token.
    response_access_token = requests.post(access_token_url, data=access_token_payload)
    response_access_token = json.loads(response_access_token.content.decode())

    headers = {'Authorization': 'Bearer {0}'.format(response_access_token['access_token'])}

    # # Step 2. Retrieve information about the current user.
    response_user_data = requests.get(people_api_url, headers=headers)
    response_user_data = json.loads(response_user_data.text)

    user = UserInfo.get_user(settings.GOOGLE_MODEL, response_user_data['sub'], response_user_data['name'],
                             response_user_data['given_name'], response_user_data['family_name'],
                             response_user_data['email'])

    token = create_token(user)
    return Response({'token': token}, status=status.HTTP_200_OK)


@api_view(['POST'])
def twitter(request):  # TODO: Twitter Login API
    request_token_url = 'https://api.twitter.com/oauth/request_token'
    access_token_url = 'https://api.twitter.com/oauth/access_token'
    print(request)
    return Response({'key': 'value'}, status=status.HTTP_200_OK)


@api_view(['POST'])
def linkedin(request):
    access_token_url = settings.LINKEDIN_ACCESS_TOKEN_URL
    people_api_url = settings.LINKEDIN_USER_DATA_URL

    body_unicode = request.body.decode('utf-8')
    body_data = json.loads(body_unicode)

    access_token_payload = dict(client_id=body_data['clientId'],
                                redirect_uri=body_data['redirectUri'],
                                client_secret=settings.LINKEDIN_SECRET_KEY,
                                code=body_data['code'],
                                grant_type='authorization_code')

    # Step 1. Exchange authorization code for access token.

    response_access_token = requests.post(access_token_url, data=access_token_payload)
    response_access_token = json.loads(response_access_token.content.decode())

    print(response_access_token)

    user_data_params = dict(oauth2_access_token=response_access_token['access_token'],
                            format='json')

    # Step 2. Retrieve information about the current user.
    response_user_data = requests.get(people_api_url, params=user_data_params)
    response_user_data = json.loads(response_user_data.text)

    user = UserInfo.get_user(settings.LINKDEIN_MODEL, response_user_data['id'],
                             response_user_data['firstName'] + ' ' + response_user_data['lastName'],
                             response_user_data['firstName'], response_user_data['lastName'],
                             response_user_data['emailAddress'])

    token = create_token(user)
    return Response({'token': token}, status=status.HTTP_200_OK)


@api_view(['POST'])
def orcid(request):
    access_token_url = settings.ORCID_ACCESS_TOKEN_URL
    people_api_url = settings.ORCID_USER_DATA_URL

    body_unicode = request.body.decode('utf-8')
    body_data = json.loads(body_unicode)

    access_token_payload = dict(client_id=body_data['clientId'],
                                redirect_uri=body_data['redirectUri'],
                                client_secret=settings.ORCID_SECRET_KEY,
                                code=body_data['code'],
                                grant_type='authorization_code')

    # # Step 1. Exchange authorization code for access token.
    response_access_token = requests.post(access_token_url, data=access_token_payload)
    response_access_token = json.loads(response_access_token.content.decode())

    user_data_params = {
        'access_token': response_access_token['access_token'],
    }

    people_api_url = people_api_url + '/' + response_access_token['orcid'] + '/' + 'record'
    headers = {'Content-Type': 'application/json'}

    # # Step 2. Retrieve information about the current user.
    response_user_data = requests.get(people_api_url, params=user_data_params, headers=headers)
    print(response_user_data)
    response_user_data = json.loads(response_user_data.content.decode())

    if not response_user_data['person']['emails']['email']:
        email = ""
    else:
        email = response_user_data['emails']['email'][0]

    print(response_user_data)
    user = UserInfo.get_user(settings.ORCID_MODEL, response_user_data['orcid-identifier']['path'],
                             response_access_token['name'],
                             response_user_data['person']['name']['given-names']['value'],
                             response_user_data['person']['name']['family-name']['value'],
                             email)

    token = create_token(user)
    return Response({'token': token}, status=status.HTTP_200_OK)


def create_token(user):
    payload = {
        'id': user.id,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(days=14)
    }
    # token = jwt.encode(payload, app.config['TOKEN_SECRET'])
    token = jwt.encode(payload, settings.SECRET_KEY)
    return token.decode('unicode_escape')


def parse_token(req):
    token = req.headers.get('Authorization').split()[1]
    return jwt.decode(token, app.config['TOKEN_SECRET'])

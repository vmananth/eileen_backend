from django.contrib.auth.models import User, Group
from rest_framework import serializers

from search.models import Vote, Document

VOTE_CHOICES = [
    ("R", "Relevant"),
    ("I", "Irrelevant")
]

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class VoteSerializer(serializers.Serializer):
    user = serializers.IntegerField(required=True)
    document_id = serializers.CharField(required=True, max_length=100)
    type = serializers.CharField(required=True, max_length=1)

    def create(self, validated_data):
        return Vote.objects.create(**validated_data)

    '''def update(self, instance, validated_data):
        instance.document_id = validated_data.get('document_id', instance.document_id)
        instance.type = validated_data.get('type', instance.type)
        instance.save()
        return instance'''

class DocumentSerializer(serializers.Serializer):
    title = serializers.CharField(required=True, max_length=50)
    identifier = serializers.CharField(required=True, max_length=100)
    source = serializers.CharField(max_length=100)
    source_id = serializers.CharField(max_length=100)
    type = serializers.CharField(max_length=30)
    venue = serializers.CharField(max_length=30)
    abstract = serializers.CharField(max_length=1000)
    scientists = serializers.CharField(max_length=100)
    organizations = serializers.CharField(max_length=30)
    date = serializers.DateField()
    end_date = serializers.DateField()
    city = serializers.CharField(max_length=20)
    country = serializers.CharField(max_length=20)
from django.conf.urls import include, url
from django.contrib import admin
from search import views as search_views
from authentication import views as auth_views


urlpatterns = [

    url(r'^admin/', include(admin.site.urls)),

    # Search Functionality Endpoints
    url(r'^search/(?P<search_string>.*)/$', search_views.search),
    url(r'^vote/$', search_views.vote),
    url(r'^recommend/$', search_views.recommend),
    url(r'^recommendationPlotData/$', search_views.extract_recommendation_words),
    url(r'^vote/clear/(?P<document_id>.*)/$', search_views.clear_vote),
    url(r'^vote/clearAll/$', search_views.clear_all),
    url(r'^dummyAPI/', search_views.dummy_api),
    url(r'^document/(?P<document_id>.*)/$', search_views.get_document),

    # # Login Authentication Functionality Endpoints
    url(r'^login/facebook/', auth_views.facebook),
    url(r'^login/google/', auth_views.google),
    url(r'^login/twitter/', auth_views.twitter),
    url(r'^login/linkedin/', auth_views.linkedin),
    url(r'^login/orcid/', auth_views.orcid),

]

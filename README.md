# eileen Backend, FrontEnd

## Details -

http://www.eileen.io/

[[http://www.eileen.io/]](http://www.eileen.io/)

Eileen uses 2 components eileen backend, eileen frontend.

- Back-end is a Django Server
- Front end is Angular JS

Source Code -

https://github.com/rgvananth/eileen_backend

https://github.com/rgvananth/eileen_frontend

Branch - development Branch (both)

The Django Server interacts with Elastic Search as the data source.

## Setup

1. Clone the repository:

````
https://github.com/rgvananth/eileen_backend.git
````

2. Install requirements
`pip install -r requirements.txt`

3. Inside the `eileen_backend` folder, execute:

````
python manage.py makemigrationspython manage.py migrate --run-syncdb
````

4. Change the configuration parameters inside `eileen_backend/eileen_backend/settings.py`


- `ELASTIC_HOST`: location of Elasticsearch server. eileen backend expects the server to have a `eileen` index
- Open Auth 2.0 Keys:
    - `FB_SECRET_KEY`: Facebook's secret key
    - `GOOGLE_SECRET_KEY` : Google's secret key
    - `LINKEDIN_SECRET_KEY`: Linkedin's secret key
    - `ORCID_SECRET_KEY`: Orcid's secret key






Running the Server -

Front End - python3 -m http.server 8008 --bind 127.0.0.1

Back-end - python manage.py runserver





## Completed -
- [ ] Include Recommendations as part of search result.

## In Progress - 
- [ ] None


## UI

Public is a universal search.

Private would recommend from your library.

![image.png](https://storage.googleapis.com/slite-api-files-production/files/b6dd680c-3be3-4a1b-9920-4d98828c1cbe/image.png)
````
